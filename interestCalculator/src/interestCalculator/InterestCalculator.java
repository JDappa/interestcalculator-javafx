package interestCalculator;

/**
 * NAME:		 Joseph Dappa
 */



//Standard fx imports
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

//Imports for components in this application
import javafx.scene.control.Label;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;

//Imports for layout
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

//Import to support quitting the application
import javafx.application.Platform;


import javafx.scene.control.DatePicker;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class InterestCalculator extends Application 
{
	//Declare components
	//Label
	private Label lblCapital, lblInterestRate, lblTerm;
	//Text field
	private TextField txtfCapital, txtfInterestRate, txtfTerm;
	//Button
	private Button btnDialog, btnClose, btnCalculate;
	//Checkboxes
	private CheckBox cbSimple, cbCompound;
	//The main text area to show the earnings analysis
	private TextArea txtArea;

	public InterestCalculator()
	{
		
	}
	@Override
	public void init()
	{
		//Instantiate componenets
		lblCapital = new Label("Capital:");
		lblInterestRate =  new Label("Interest Rate(%):");
		lblTerm = new Label("Term(yrs):");
		//Text field
		txtfCapital = new TextField();
		txtfInterestRate = new TextField();
		txtfTerm = new TextField();
		//Button
		btnDialog = new Button("...");
		btnClose = new Button("Close");
		btnCalculate = new Button("Calculate");
		
		//Manage button size
		btnClose.setMinWidth(70);
		btnCalculate.setMinWidth(70);
		
		//Close application
		btnClose.setOnAction(ae->Platform.exit());
		
		//Checkboxes
		cbSimple = new CheckBox("Simple Interest");
		cbCompound = new CheckBox("Compound Interest");
		//The main text area to show the earnings analysis
		txtArea = new TextArea ();
		
		//Manage the term textfield
		txtfTerm.setMaxWidth(50);

		//Event handler
		//Pick date
		btnDialog.setOnAction(ae->showTermDialog());
		
		//Handle events on the calculate button
		btnCalculate.setOnAction(ae->{
		
			//Get value for capital, interest rate, term
			double cap = Double.parseDouble(txtfCapital.getText());
			double intRate = Double.parseDouble(txtfInterestRate.getText());
			double invTerm = Double.parseDouble(txtfTerm.getText());
			
			//Check the state of checknoxes
			if(cbSimple.isSelected())
				showSimpleInterest(cap,intRate,invTerm);
			if(cbCompound.isSelected())
			{
				//Show the interest earnings analysis
				showCompoundInterest(cap,intRate,invTerm);
			}
			
			
			//calculate the interest accordingly.
			
			//show the interest earnings analysis
		
		});
		
	}//end of init
	
	//Method to calculate simple interest
	private double getSimpleInterest(double amt, double iRate, double yrs)
	{
		double interest = 0;
		//THe interest is the amount X iRate/100 X yrs
		interest = amt * (iRate/100) * yrs;
		return interest; 
	}//end of getSimpleInterest
	
	//Method to show simple interest analysis in the text area
	private void showSimpleInterest(double amt, double iRate, double yrs)
	{
		//local variables
		double interest = 0;
		double increasedCapital = 0;
		String analysisString = "";
		
		//Use a call to getSimpleInterest().
		double interestAmount = getSimpleInterest(amt, iRate, yrs);
		
		//Get the increased capital (with interest applied)
		increasedCapital = amt + interestAmount;
		
		//Construct the analysis strin.
		analysisString +="Simple Interest: \nYear "+ yrs+
				"\nInitial Capital: " +amt+
				"\nInterest earned: "+interestAmount+
				"\nFinal amount: "+increasedCapital;
		//show the analysis string on text area
		txtArea.setText(analysisString);
	}//end of showSimpleInterest
	
	//Show compound interest method
	private void showCompoundInterest(double amt, double iRate, double yrs)
	{
		//local variables
		double interestAmt = 0;
		double increasedCapital = amt;
		double totalInterest = 0;
		String analysisString = "";
		
		//Iteratively calculate interest for each year
		for(int i = 1;i<=yrs;i++)
		{
			//The interest is the amount x iRate/100 x years.
			interestAmt = getSimpleInterest(increasedCapital, iRate, 1);
			totalInterest = totalInterest + interestAmt;
			increasedCapital = increasedCapital + interestAmt;
			
		}//end of for loop
		
		//Now create the analysis string for display
		
		analysisString = "\n\nCompound Interest:\nYear: "+yrs+
				"\nInitial capital: "+amt+
				"\nInterest earned: "+totalInterest+
				"\nFinal amount: "+increasedCapital;
		
		//Now show the analysis string in the text area
		txtArea.setText(txtArea.getText() + analysisString);
	}
	

	@Override
	public void start(Stage pStage) throws Exception 
	{
		//Set the title
		pStage.setTitle("Interest Calculator V1.1.0");
		//Set the width and height
		pStage.setWidth(400);
		pStage.setHeight(400);
		
		//Create layout
		GridPane gp = new GridPane();
		
		//A vertica box is the main container
		VBox vbMain = new VBox();
		
		//Add the gp into the box
		vbMain.getChildren().add(gp);
		
		//Add main text area to vbmain
		vbMain.getChildren().add(txtArea);
		
		//Add the buttons to HBOX
		HBox hbButtons = new HBox();
		hbButtons.getChildren().addAll(btnClose,btnCalculate);
		hbButtons.setAlignment(Pos.BASELINE_RIGHT);
		
		//Set spacing in buttons
		hbButtons.setSpacing(10);
		vbMain.getChildren().add(hbButtons);
		
		//Set the padding for vbMain
		vbMain.setPadding(new Insets(10));
		vbMain.setSpacing(10);
		
		//Set the hgap and vgap for gridpane
		gp.setHgap(10);
		gp.setVgap(10);
		
	
		//HBox for the term components
		HBox hbTerm = new HBox();
		
		//Add the term components
		hbTerm.getChildren().addAll(txtfTerm,btnDialog);
		
		//Add the term components
		hbTerm.setSpacing(10);
		//Set the alignment for the gp
		gp.setAlignment(Pos.CENTER_RIGHT);
		
		//add component to the layout
		gp.add(lblCapital, 0, 0);
		gp.add(txtfCapital, 1, 0);
		
		gp.add(lblInterestRate, 0, 1);
		gp.add(txtfInterestRate, 1, 1);
		
		gp.add(lblTerm, 0, 2);
		gp.add(hbTerm, 1, 2);
		
		gp.add(cbSimple, 1, 3);
		gp.add(cbCompound, 1, 4);
		
		//Create scene
		Scene s = new Scene(vbMain);
		
		//Set scene
		pStage.setScene(s);
		
		//show stage
		pStage.show();

	}
	
	//Pick the Date
	public void showTermDialog()
	{
		//Create stage
		Stage termDialogStage = new Stage();
		//Create title
		termDialogStage.setTitle("Select investment term");
		//Create vbox layout
		GridPane gpDialog = new GridPane();
		gpDialog.setHgap(10);
		gpDialog.setVgap(10);
		gpDialog.setPadding(new Insets(10));
		
		//Create HBOX
		HBox hbButtons = new HBox();
		//Create control instances and add them to the layout
		Label lblTermStart = new Label("Investment start date:");
		Label lblTermEnd = new Label("Investment end date:");
		
		//Create datepicker
		DatePicker dpTermStart = new DatePicker();
		DatePicker dpTermEnd = new DatePicker();
		
		//Disable datepicker editable
		dpTermStart.setEditable(false);
		dpTermEnd.setEditable(false);
		
		//Buttons standard for OK and Cancel
		Button btnOK = new Button("OK");
		Button btnCancel = new Button("Cancel");
		
		//Manage button sizes
		btnOK.setMinWidth(70);
		btnCancel.setMinWidth(70);
		btnOK.setDisable(true);
		
		//Add the buttons to the hbox
		hbButtons.getChildren().addAll(btnCancel,btnOK);
		
		//Manage hbox spacing and pos
		hbButtons.setSpacing(10);
		hbButtons.setPadding(new Insets(10));
		hbButtons.setAlignment(Pos.BASELINE_RIGHT);
		
		//Add components to gpDialog
		gpDialog.add(lblTermStart,0,0);
		gpDialog.add(dpTermStart,1,0);
		
		gpDialog.add(lblTermEnd,0,1);
		gpDialog.add(dpTermEnd,1,1);
		
		//Next row...
		gpDialog.add(hbButtons,1,2);
		
		//Handle events
		//The cancel button. Do nothing. Just close the dialog
		btnCancel.setOnAction(ae->termDialogStage.close());
		
		//The OK button. Dialog confirmed. Commit user selection.
		btnOK.setOnAction(ae->{
			LocalDate startDate = dpTermStart.getValue();
			LocalDate endDate = dpTermEnd.getValue();
			//Check order of the date enter by the user
			if(startDate.isBefore(endDate))
			{
				//Get the years between the dates
				long years = ChronoUnit.YEARS.between(startDate,endDate);
				
				//Populate the investment term textfield back on main.
				txtfTerm.setText(""+years);
				
				//Close the dialog
				termDialogStage.close();
			}
			else
			{
				Alert a = new Alert(AlertType.INFORMATION);
				a.setTitle("Information Dialog");
				a.setHeaderText("Date Error");
				a.setContentText("Start Date should be earlier than End Date.");
				a.show();
				
			}
			
			
		});
		
		//Handle events on the datepicker. Don't allow allow null input
		dpTermEnd.setOnMouseClicked(ae->{
			if(dpTermStart.getValue()==null)
				btnOK.setDisable(true);
			else
				btnOK.setDisable(false);
		});
		
		dpTermStart.setOnMouseClicked(ae->{
			if(dpTermEnd.getValue()==null)
				btnOK.setDisable(true);
			else
				btnOK.setDisable(false);
		});
	
		
		//Create a scene
		Scene s = new Scene(gpDialog);
		//Set a scene
		termDialogStage.setScene(s);

		//show stage
		termDialogStage.show();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		launch();
	}

}
